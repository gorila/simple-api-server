<?php

function get_env_var($key = '', $default = null) {
    return $_ENV[$key] ?? $default;
}
