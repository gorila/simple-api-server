<?php

namespace App;

class Routes {
	public static function dispatch($router) {
		$router->map('GET', '/apps/plugin-instance/', 'App\Controllers\HomeController::response');
		$router->map('GET', '/apps/plugin-instance/ping', 'App\Controllers\HomeController::response');
		$router->map('POST', '/apps/plugin-instance/register', 'App\Controllers\RegisterController::response');
		return $router;
	}
}