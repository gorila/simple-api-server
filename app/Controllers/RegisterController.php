<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ApiErrorException;

class RegisterController extends Controller {
	public function response() {

		return [
	        'title'   => 'My New Simple API',
	        'version' => 1,
	    ];
	}
}