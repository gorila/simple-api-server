<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{
    protected $table = 'users';
    protected $primaryKey = 'ID';

    protected $dates = [
        'user_registered', 
    ];

    protected $hidden = [
        'user_pass',
        'user_activation_key', 
    ];

    protected $fillable = [
        'user_login',
		'user_pass',
		'user_nicename',
		'user_email',
		'user_url',
		'user_registered',
		'user_activation_key',
		'user_status',
		'display_name', 
    ];
}

