<?php 

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\{RedirectResponse,JsonResponse};

class SlashMiddleware implements MiddlewareInterface
{
	protected $exception;

    public function __construct($exception) {
    	$this->exception = $exception;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {

    	$baseUrl = get_env_var('APP_BASE_URL');
    	$path = str_replace('/apps/plugin-instance/', '', $request->getUri()->getPath() );

    	if (!empty($path) && substr($path, -1) === '/') {
	    	$baseUrl .= substr($path, 0, strlen($path) - 1);

	    	$query = $request->getUri()->getQuery();

	    	if (!empty($query)) {
	    		$baseUrl .= '?' . $query;
	    	}

	    	if (!empty($baseUrl)) {
	    		return new RedirectResponse($baseUrl);
	    	}
    	}

    	$data = [
    		'success' => false,
    		'response_code' => 404,
    		'message' => 'Page not found',
    	];
    	
    	$response = new JsonResponse($data);
		return $response;
    }
}