<?php

namespace App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class Framework {
    public function start() {
        // Buat instance ServerRequest dari globals
        $request = \Laminas\Diactoros\ServerRequestFactory::fromGlobals(
            $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
        );

        // Buat factory untuk response
        $responseFactory = new \Laminas\Diactoros\ResponseFactory();

        // Buat strategi output
        $strategy = new OutputStrategy($responseFactory);

        // Buat router
        $routerEngine = (new \League\Route\Router)->setStrategy($strategy);

        $router = Routes::dispatch($routerEngine);

        // Dispatch permintaan dan dapatkan respons
        $response = $router->dispatch($request);

        // Emitter respons menggunakan SapiEmitter
        (new \Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
    }
}
