<?php

ini_set('display_errors', 'true');
ini_set('display_startup_errors', 'true');
error_reporting(E_ALL);

define('BASEPATH', __DIR__ );

require_once BASEPATH.'/vendor/autoload.php';
$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

date_default_timezone_set(get_env_var('APP_TIMEZONE', 'Asia/Jakarta'));
ini_set('date.timezone', get_env_var('APP_TIMEZONE', 'Asia/Jakarta'));
date_default_timezone_set(get_env_var('APP_TIMEZONE', 'Asia/Jakarta'));

use Illuminate\Database\Capsule\Manager as Capsule;


$capsule = new Capsule;

$capsule->addConnection([
	'driver'	=> 'mysql',
	'host'		=> get_env_var('MYSQL_HOST'),
	'database'	=> get_env_var('MYSQL_NAME'),
	'username'	=> get_env_var('MYSQL_USER'),
	'password'	=> get_env_var('MYSQL_PASS'),
	'prefix'	=> get_env_var('MYSQL_PREFIX'),
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

$app = new \App\Framework;

$app->start();